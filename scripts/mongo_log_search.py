
from pymongo import MongoClient
import datetime


def search_by_date():

    from_date = input("Enter From Date \'YYYY-MM-DD'\': ")
    from_date = datetime.datetime.strptime(from_date, '%Y-%m-%d')

    to_date = input("Enter To Date \'YYYY-MM-DD'\': ")
    to_date = datetime.datetime.strptime(to_date, '%Y-%m-%d')

    resp_dict = list()
    for bson_obj in db.API.find({"datetime": {
        "$gte": from_date,
        "$lt": to_date
    }}):
        resp_dict.append(bson_obj)

    return resp_dict


def full_text_search():
    return ["COMING SOON"]


def top_n_requests():
    limit_value = int(input("\nEnter the int:n-value :"))
    resp_dict = list()
    for bson_obj in db.API.find().limit(limit_value):
        resp_dict.append(bson_obj)

    return resp_dict


def request_type():
    flag = True
    valid_options = ['error', 'warning', 'exception', 'success']

    print("Valid Options: {}".format(valid_options))
    while flag:
        method = input("\nEnter the request type :")
        if method.lower() in valid_options:
            flag = False
        else:
            print("Invalid Choice.Valid Options: {}".format(valid_options))

    resp_dict = list()
    for bson_obj in db.API.find({"type": method.lower()}):
        resp_dict.append(bson_obj)

    return resp_dict


def request_method():

    flag = True
    valid_options = ['GET', 'POST', 'DELETE', 'PATCH']

    print("Valid Options: {}".format(valid_options))
    while flag:
        method = input("\nEnter the method type :")
        if method.upper() in valid_options:
            flag = False
        else:
            print("Invalid Choice.\nValid Options: {}".format(valid_options))

    resp_dict = list()
    for bson_obj in db.API.find({"method": method.upper()}):
        resp_dict.append(bson_obj)

    return resp_dict


def menu():
    option = {
        1: (request_method, "Request Method Search"),
        2: (top_n_requests, "Top N-Search"),
        3: (full_text_search, "Full Text Search"),
        4: (request_type, "Request Type Search"),
        5: (search_by_date, "Search By Date-Range")
    }
    return option


def run():

    client = MongoClient("mongodb://localhost")
    try:
        assert "log_db" in client.list_database_names()
    except AssertionError:
        raise AssertionError("DB \'log_db\' doesn't exist.")

    global db
    db = client["log_db"]

    try:
        assert "API" in db.list_collection_names()
    except AssertionError:
        raise AssertionError("collection \'API\' doesn't exist in DB: log_db")

    menu_option = menu()

    for key, value in menu_option.items():
        print("{}. {}".format(key, value[1]))

    option = int(input("Choose : "))
    if option in [key for key, value in menu_option.items()]:
        data = menu_option.get(option)[0]()

        print(data)
        print(len(data))
