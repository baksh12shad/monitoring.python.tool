import sys
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings as django_settings
from .connection import SettingUpDatabase


class DjangoLoggingSettings(object):
    def __init__(self):

        user_settings = getattr(django_settings, 'MONITOR_INFO', None)
        """
                    DB = {
                            'db_type': 'cassandra',
                            'keyspace': 'log_db',
                            'table': 'api',
                        }
        """

        try:
            assert user_settings.get('DB', False)
        except Exception as e:
            raise ImproperlyConfigured("\'database is not properly configured. Check API doc.\'")


        self.__settings = dict(
            # DB={
            #     'db_type': 'mongo',
            #     'db_name': 'log_db',
            #     'port': None,
            # },
            # DB={
            #     'db_type': 'cassandra',
            #     'keyspace': 'log_db',
            #     'table': 'api',
            # },

            DISABLE_EXISTING_LOGGERS=True,
            IGNORED_PATHS=['/admin', '/static', '/favicon.ico'],
            LOGGING_FIELDS=('status', 'reason', 'charset', 'headers', 'content'),
            CONTENT_JSON_ONLY=True,
            CONTENT_TYPES=None,
            ENCODING='utf-8',
            ROTATE_MB=100,
            ROTATE_COUNT=10,
            MAX_RESPOSE_CONTENT_SIZE=3000,       # it is in bytes

            CASSANDRA_KEYSPACE_NAME=None,
            CASSANDRA_TABLE_NAME=None,
            MONGO_DB_NAME=None,
            MONGO_COLLECTION_NAME=None,
            DB_CONNECTION=None
        )

        updated_settings_file = SettingUpDatabase(self.__settings, db=user_settings.get('DB'))
        self.__settings.update(updated_settings_file.settings)

        try:
            self.__settings.update(user_settings)
        except TypeError:
            pass

        if self.CONTENT_JSON_ONLY:
            self.__settings['CONTENT_TYPES'] = self.CONTENT_TYPES or []
            self.__settings['CONTENT_TYPES'].append('application/json')

        if self.SQL_LOG:
            self.setup_sql_logging()

    def __getattr__(self, name):
        return self.__settings.get(name)


sys.modules[__name__] = DjangoLoggingSettings()
