from .db_handler import CassandraConnection, MongoConnection


def setup_cassandra(settings_file, keyspace='log_db', table='api', **kwargs):
    settings_file['CASSANDRA_KEYSPACE_NAME'] = keyspace
    settings_file['CASSANDRA_TABLE_NAME'] = table
    settings_file['DB_CONNECTION'] = CassandraConnection
    print("CASSANDRA : meta data setup is done!")
    return settings_file


def setup_mongo(settings_file, db_name='log_db', collection_name='API', **kwargs):
    settings_file['MONGO_DB_NAME'] = db_name
    settings_file['MONGO_COLLECTION_NAME'] = collection_name
    settings_file['DB_CONNECTION'] = MongoConnection
    print("MONGO : meta data setup is done!")
    return settings_file


db_map = {
    'mongo': setup_mongo,
    'cassandra': setup_cassandra,
}


class SettingUpDatabase(object):
    def __init__(self, settings_file, db):
        self.db_type = db.get('db_type')
        if self.db_type not in db_map.keys():
            raise ValueError("This db is not supported.")

        self.settings = db_map.get(self.db_type)(settings_file=settings_file, **db)


